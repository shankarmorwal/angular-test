import EntitiesListingModule from './entitiesListing'
import EntitiesListingController from './entitiesListing.controller';
import EntitiesListingComponent from './entitiesListing.component';
import EntitiesListingTemplate from './entitiesListing.html';

describe('EntitiesListing', () => {
  let $rootScope, makeController;

  beforeEach(window.module(EntitiesListingModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new EntitiesListingController();
    };
  }));

  describe('Template', () => {
      it('renders entity-list component', () => {
         expect(EntitiesListingTemplate).to.match(/<entities-list/g);
      });

      it('passes entitieslist to entity-list component', () => {
          expect(EntitiesListingTemplate).to.match(/entities="vm\.entitiesList"/g);
      });
  });

  describe('Component', () => {
      // component/directive specs
      let component = EntitiesListingComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(EntitiesListingTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(EntitiesListingController);
      });
  });
});
