import template from './entitiesListing.html';
import controller from './entitiesListing.controller';

let entitiesListingComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default entitiesListingComponent;
