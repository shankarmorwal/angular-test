import angular                  from 'angular';
import entitiesListingComponent    from './entitiesListing.component';
import EntitiesList                from '../../components/entitiesList/entitiesList';

let entitiesListingModule = angular.module('entitiesListing', [
    EntitiesList.name
])

.component('entitiesListing', entitiesListingComponent);

export default entitiesListingModule;
