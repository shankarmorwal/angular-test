class EntitiesListingController {
  constructor(EntitiesService) {
    "ngInject";

    // This will keep the service instance across our class
    this.EntitiesService = EntitiesService;

    // this will gold our entitiesList, it will be passed to the other components.
    this.entitiesList = [];
  }

  // This method will be called each time the component will be initialised,
  // In our case, it will be called for every page route change.
  $onInit(){
    this.entitiesList = this.EntitiesService.getEntities();
  }
}

export default EntitiesListingController;
