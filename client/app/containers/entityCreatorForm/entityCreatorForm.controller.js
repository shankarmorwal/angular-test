class GotCreatorFormController {
  constructor($state, EntitiesService) {
    "ngInject";

    this.$state       = $state;
    this.EntitiesService = EntitiesService;

    this.entities = this.EntitiesService.getEntities();
  }

    connectEntity() {
        this.EntitiesService.setEntities(this.entities);
        this.hideModal();
        this.$state.go('app.entity');
    }

    hideModal() {
        var modalInstance = this.EntitiesService.getModalInstance();
        modalInstance.dismiss('cancel');
    }
}

export default GotCreatorFormController;
