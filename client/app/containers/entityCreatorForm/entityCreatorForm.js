import angular                  from 'angular';
import entityCreatorFormComponent from './entityCreatorForm.component';

import EntityListItemComponent from '../../components/entityListItem/entityListItem';

let gotCreatorFormModule = angular.module('gotCreatorForm', [
    EntityListItemComponent.name
])

.component('entityCreatorForm', entityCreatorFormComponent);

export default gotCreatorFormModule;
