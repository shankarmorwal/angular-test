import EntitiesListModule from './entitiesList'
import EntitiesListController from './entitiesList.controller';
import EntitiesListComponent from './entitiesList.component';
import EntitiesListTemplate from './entitiesList.html';

describe('EntitiesList', () => {
  let $rootScope, makeController;

  beforeEach(window.module(EntitiesListModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new EntitiesListController();
    };
  }));

  describe('Template', () => {
    it('It renders the goat-list-item directive', () => {
      expect(EntitiesListTemplate).to.match(/goat-list-item/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      let component = EntitiesListComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(EntitiesListTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(EntitiesListController);
      });

      it('expose entities as one way input binding', () => {
         expect(component.bindings.entities).to.equal('<');
      });
  });
});
