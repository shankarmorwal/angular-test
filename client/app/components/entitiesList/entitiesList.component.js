import template from './entitiesList.html';
import controller from './entitiesList.controller';

let entitiesListComponent = {
  restrict: 'E',
  bindings: {
      entities: '<'
  },
  template,
  controller,
  controllerAs: 'vm'

};

export default entitiesListComponent;
