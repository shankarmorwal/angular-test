import angular              from 'angular';
import entitiesListComponent   from './entitiesList.component';

import EntityListItemComponent from '../entityListItem/entityListItem';

const entitiesListModule = angular.module('entitiesList', [
    EntityListItemComponent.name
])

.component('entitiesList', entitiesListComponent);

export default entitiesListModule;
