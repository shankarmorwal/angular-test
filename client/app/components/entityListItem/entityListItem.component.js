import template   from './entityListItem.html';
import controller from './entityListItem.controller';
import './entityListItem.scss';

const entityListItemComponent = {
  restrict: 'E',
  bindings: {
    entity: '<'
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default entityListItemComponent;
