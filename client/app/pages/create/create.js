import angular          from 'angular';
import createComponent  from './create.component';
import EntityCreateForm   from '../../containers/entityCreatorForm/entityCreatorForm';

const createModule = angular.module('create', [
    EntityCreateForm.name
])

.component('create', createComponent);

export default createModule;
