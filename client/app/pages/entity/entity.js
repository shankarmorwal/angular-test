import angular                  from 'angular';
import entityComponent            from './entity.component';
import EntitiesListingComponent    from '../../containers/entitiesListing/entitiesListing';

let entityModule = angular.module('entity', [
    EntitiesListingComponent.name
])

.component('entity', entityComponent);

export default entityModule;
