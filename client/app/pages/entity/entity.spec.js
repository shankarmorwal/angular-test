import EntityModule       from './entity'
import EntityController   from './entity.controller';
import EntityComponent    from './entity.component';
import EntityTemplate     from './entity.html';

describe('Entity', () => {
  let $rootScope, makeController;

  beforeEach(window.module(EntityModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new EntityController();
    };
  }));

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('renders goats listing component', () => {
      expect(EntityTemplate).to.match(/<goats-listing>/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      let component = EntityComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(EntityTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(EntityController);
      });
  });
});
