class EntityController {
  constructor(EntitiesService) {

        "ngInject";

        this.name = 'List Entity';
        this.EntitiesService = EntitiesService;
        this.entities = this.EntitiesService.getConnectedEntities();
  }
}

export default EntityController;
