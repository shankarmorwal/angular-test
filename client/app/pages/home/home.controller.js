class HomeController {
  constructor($uibModal, EntitiesService) {
        "ngInject";
    this.name = 'home';
    this.$uibModal = $uibModal;
    this.EntitiesService = EntitiesService;
  }

    showModal() {
        var modalInstance = this.$uibModal.open({
            template: '<entity-creator-form></entity-creator-form>',
            backdrop: 'static',
            bindings: { // Previous resolve property (I assume)
               // myAttribute: 'myStringReferenceTo$scopeVariable'
            }
        });
        this.EntitiesService.setModalInstance(modalInstance);
    }
}

export default HomeController;
