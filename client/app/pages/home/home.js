import angular                  from 'angular';
import homeComponent            from './home.component';
import EntitiesListingComponent    from '../../containers/entitiesListing/entitiesListing';

let homeModule = angular.module('home', [
    EntitiesListingComponent.name
])

.component('home', homeComponent);

export default homeModule;
