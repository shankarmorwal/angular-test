function EntitiesService() {
    "ngInject";

    // private variable to store our entities entries
    var entities = [
            {'name' : 'Arrivals', 'type' : 'BPM', isConnected : false},
            {'name' : 'Car Wash', 'type' : 'BPM', isConnected : false},
            {'name' : 'Maintenance', 'type' : 'Project', isConnected : false},
            {'name' : 'Customer Payment', 'type' : 'BPM', isConnected : false},
            {'name' : 'Technical Issues', 'type' : 'Project', isConnected : false},
            {'name' : 'Arrivals Database', 'type' : 'Database', isConnected : true},
            {'name' : 'Calculator', 'type' : 'Application', isConnected : true},
            {'name' : 'California', 'type' : 'Server', isConnected : true}
    ];
    var modalInstance;

    return {

        // Will retrieve our entities list for displaying
        getEntities() {
            return entities;
        },

        getConnectedEntities() {
            var tmp = angular.copy(entities);
            var connectedEntities = [];

            for(var k=0; k < tmp.length; k++){
                if(tmp[k].isConnected){
                    connectedEntities.push(tmp[k]);
                }
            }
            return connectedEntities;
        },

        // Creating a new goat entry based on user input.
        setEntities(e) {
            entities = e;
        },

    getModalInstance() {
        return modalInstance;
    },

    // Creating a new goat entry based on user input.
    setModalInstance(e) {
        modalInstance = e;
    }
    }
}

export default EntitiesService;