import angular      from 'angular';
import uiRouter     from 'angular-ui-router';
import uiBootstrap      from 'angular-ui-bootstrap';

import AppComponent from './app.component';




import NavigationComponent from './components/navigation/navigation';

import HomeComponent from './pages/home/home';
import CreateComponent from './pages/create/create';
import EntityComponent from './pages/entity/entity';
import EntitiesService from './services/EntitiesService';

// import our default styles for the whole application
import 'normalize.css';
import 'bootstrap/dist/css/bootstrap.css';
import '../theme/mixins.scss';
import '../theme/variables.scss';


angular
    .module('app', [
        uiRouter,
        uiBootstrap,
        NavigationComponent.name,
        CreateComponent.name,
        HomeComponent.name,
        EntityComponent.name
    ])
    .config(($locationProvider, $stateProvider, $urlRouterProvider) => {
        "ngInject";

        // Define our app routing, we will keep our layout inside the app component
        // The layout route will be abstract and it will hold all of our app views
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                template: '<app></app>'
            })

            // Dashboard page to contain our goats list page
            .state('app.home', {
                url: '/home',
                template: '<home></home>'
            })

            .state('app.entity', {
                url: '/entity',
                template: '<entity></entity>'
            })

            // Create route for our goat listings creator
            .state('app.create', {
                url: '/create',
                template: '<create></create>'
            });

        $urlRouterProvider.otherwise('/app/home');
    })
    .component('app', AppComponent)
    .factory('EntitiesService', EntitiesService);
